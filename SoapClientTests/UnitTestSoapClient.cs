﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SoapConnector;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace SoapClientTests
{
    [TestClass]
    public class UnitTestSoapClient
    {
        private SoapClient client;

        [TestInitialize]
        public void Initialize()
        {
            this.client = new SoapClient(this.LoadSoapBaseUrl(), "", "");
            this.client.Initialize();
        }

        [TestMethod]
        public void TestClientInstantiation()
        {
            Assert.IsTrue(client.IsInitialized());
        }

        [TestMethod]
        public void TestGetMethodParameters()
        {
            Assert.IsNotNull(this.client.GetMethodParameters("Login_Post"));
        }

        [TestMethod]
        public void TestInvokeMethod()
        {
            var methodparams = this.client.GetMethodParameters("Catalog_Get");

            var parameters = new Dictionary<string, object>();
            parameters.Add("filters", new string[] { });
            parameters.Add("page", 1);

            WebServiceReturnObjectSchema response = this.client.InvokeMethod("Catalog_Get", new object[] { parameters }) as WebServiceReturnObjectSchema;
        }

        protected string LoadSoapBaseUrl()
        {
            using (var reader = new StreamReader("config.yaml"))
            {
                var deserializer = new DeserializerBuilder()
                    .WithNamingConvention(new CamelCaseNamingConvention())
                    .Build();

                var config = (Dictionary<string, string>)deserializer.Deserialize(reader, typeof(Dictionary<string, string>));

                var result = string.Empty;
                config.TryGetValue("urlBase", out result);

                return result;
            }
        }
    }
}
