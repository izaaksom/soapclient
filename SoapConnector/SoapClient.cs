﻿using Microsoft.CSharp;
using Newtonsoft.Json;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace SoapConnector
{
    public class SoapClient : ISoapClient
    {
        /// <summary>
        /// 
        /// </summary>
        private string BaseUrl;

        /// <summary>
        /// 
        /// </summary>
        private string Username;

        /// <summary>
        /// 
        /// </summary>
        private string Password;

        /// <summary>
        /// 
        /// </summary>
        private string AssemblyPath;

        /// <summary>
        /// 
        /// </summary>
        private Type[] AssemblyTypes;

        /// <summary>
        /// 
        /// </summary>
        private object WebServiceProxy;

        /// <summary>
        /// SoapClient class constructor.
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public SoapClient(string baseUrl, string username, string password)
        {
            BaseUrl = baseUrl;
            Username = username;
            Password = password;
        }

        /// <inheritdoc />
        public object InvokeMethod(string methodName, object[] parameters)
        {
            MethodInfo methodInfo = WebServiceProxy.GetType().GetMethod(methodName);

            object[] serializedParams = parameters.Select(i => JsonConvert.SerializeObject(i)).ToArray();

            object response = methodInfo.Invoke(WebServiceProxy, serializedParams);

            return JsonConvert.DeserializeObject<WebServiceReturnObjectSchema>(response.ToString());

        }

        /// <inheritdoc />
        public ParameterInfo[] GetMethodParameters(string methodName)
        {
            var method = this.WebServiceProxy.GetType().GetMethod(methodName);

            return method.GetParameters();
        }

        /// <inheritdoc />
        public ISoapClient Initialize()
        {
            Stream stream = this.GetWsdlStream();
            ServiceDescription description = ServiceDescription.Read(stream);

            ServiceDescriptionImporter descriptionImporter = new ServiceDescriptionImporter();
            descriptionImporter.ProtocolName = Enum.GetName(typeof(SoapProtocolVersion), SoapProtocolVersion.Soap12);
            descriptionImporter.AddServiceDescription(description, null, null);
            descriptionImporter.Style = ServiceDescriptionImportStyle.Client;
            descriptionImporter.CodeGenerationOptions = CodeGenerationOptions.GenerateProperties;

            CodeNamespace codeNamespace = new CodeNamespace();
            CodeCompileUnit codeCompileUnit = new CodeCompileUnit();
            codeCompileUnit.Namespaces.Add(codeNamespace);

            ServiceDescriptionImportWarnings serviceDescriptionImportWarnings
                = descriptionImporter.Import(codeNamespace, codeCompileUnit);
            if (serviceDescriptionImportWarnings == ServiceDescriptionImportWarnings.NoCodeGenerated)
            {
                throw new Exception("Error while importing service.");
            }

            CodeDomProvider compiler = new CSharpCodeProvider();
            string[] references = new[] { "System.Web.Services.dll", "System.Xml.dll" };
            CompilerParameters parameters = new CompilerParameters(references);
            parameters.TreatWarningsAsErrors = true;
            parameters.OutputAssembly = "SoapClient.dll";
            CompilerResults results = compiler.CompileAssemblyFromDom(parameters, codeCompileUnit);

            if (results.Errors.HasErrors)
            {
                throw new Exception("Error while importing service.");
            }

            this.AssemblyPath = results.PathToAssembly;
            this.AssemblyTypes = results.CompiledAssembly.GetExportedTypes();
            this.WebServiceProxy = Activator.CreateInstance(this.AssemblyTypes.First());

            return this;
        }

        /// <inheritdoc />
        public bool IsInitialized()
        {
            return this.WebServiceProxy != null;
        }

        /// <inheritdoc />
        public ISoapClient UseEncryption(bool useEncryption)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns a definition stream from the WSDL URI.
        /// </summary>
        /// <returns></returns>
        protected Stream GetWsdlStream()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(this.BaseUrl);
            request.Credentials = CredentialCache.DefaultCredentials;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            return response.GetResponseStream();
        }
    }
}
