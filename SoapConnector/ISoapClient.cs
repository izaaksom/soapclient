﻿using System.Reflection;

namespace SoapConnector
{
    public interface ISoapClient
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="endpointName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        object InvokeMethod(string endpointName, object[] parameters);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodName"></param>
        /// <returns></returns>
        ParameterInfo[] GetMethodParameters(string methodName);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ISoapClient Initialize();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool IsInitialized();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="useEncryption"></param>
        /// <returns></returns>
        ISoapClient UseEncryption(bool useEncryption);
    }
}
