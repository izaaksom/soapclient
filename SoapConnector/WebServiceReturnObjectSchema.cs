﻿using System.Collections.Generic;

namespace SoapConnector
{
    public class WebServiceReturnObjectSchema
    {
        public object return_value { get; set; }

        public bool has_exception { get; set; }

        public string ExceptionMessage { get; set; }

        public List<string> ERRORS { get; set; }

        public bool noSessionException { get; set; }
    }
}
